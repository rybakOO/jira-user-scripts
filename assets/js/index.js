window.JIRA = {};

var subscribes = {}

JIRA.bind = function(event, fn) {
  if(!Array.isArray(subscribes[event])) subscribes[event] = [];
  subscribes[event].push(fn);
};

JIRA.unbind = function(event, fn) {
  if(!Array.isArray(subscribes[event])) subscribes[event] = [];
  subscribes[event] = subscribes[event].filter(function(_fn) {
    return _fn !== fn;
  });
};

JIRA.trigger = function(event) {
  if(!Array.isArray(subscribes[event])) subscribes[event] = [];
  subscribes[event].forEach((fn) => fn());
};

JIRA.Events = {
  NEW_CONTENT_ADDED: "newContentAdded",
  NEW_PAGE_ADDED: "newPageAdded",
  REFRESH_TOGGLE_BLOCKS: "refreshToggleBlocks",
  CHECKBOXMULITSELECT_READY: "checkboxMultiSelectReady",
  VALIDATE_TIMETRACKING: "validateTimeTracking",
  ISSUE_REFRESHED: "issueRefreshed",
  PANEL_REFRESHED: "panelRefreshed",
  INLINE_EDIT_SAVE_COMPLETE: "inlineEditSaveComplete",
  INLINE_EDIT_STARTED: "inlineEditStarted",
  BEFORE_INLINE_EDIT_CANCEL: "inlineEditCancelled",
  INLINE_EDIT_BLURRED: "inlineEditBlurred",
  INLINE_EDIT_FOCUSED: "inlineEditFocused",
  INLINE_EDIT_REQUESTED: "inlineEditRequested",
  LOCK_PANEL_REFRESHING: "lockPanelRefreshing",
  UNLOCK_PANEL_REFRESHING: "unlockPanelRefreshing",
  REFRESH_ISSUE_PAGE: "refreshIssuePage"
};

window.onload = function() {
  var triggerObserver = document.querySelector('#trigger-observer');
  triggerObserver.addEventListener('click', function() {
    var el = document.querySelector('#issue-content');
    var nestedEl = document.createElement('div');
    el.append(nestedEl);
  });
}