type IEvent = {
  NEW_CONTENT_ADDED: "newContentAdded",
  NEW_PAGE_ADDED: "newPageAdded",
  REFRESH_TOGGLE_BLOCKS: "refreshToggleBlocks",
  CHECKBOXMULITSELECT_READY: "checkboxMultiSelectReady",
  VALIDATE_TIMETRACKING: "validateTimeTracking",
  ISSUE_REFRESHED: "issueRefreshed",
  PANEL_REFRESHED: "panelRefreshed",
  INLINE_EDIT_SAVE_COMPLETE: "inlineEditSaveComplete",
  INLINE_EDIT_STARTED: "inlineEditStarted",
  BEFORE_INLINE_EDIT_CANCEL: "inlineEditCancelled",
  INLINE_EDIT_BLURRED: "inlineEditBlurred",
  INLINE_EDIT_FOCUSED: "inlineEditFocused",
  INLINE_EDIT_REQUESTED: "inlineEditRequested",
  LOCK_PANEL_REFRESHING: "lockPanelRefreshing",
  UNLOCK_PANEL_REFRESHING: "unlockPanelRefreshing",
  REFRESH_ISSUE_PAGE: "refreshIssuePage"
}

interface IJIRA {
  Events: IEvent,
  bind: (event: string, fn: any) => void,
  unbind: (event: string, fn: any) => void,
}

declare const JIRA: IJIRA;