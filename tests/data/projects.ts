export default {
  fields: [
    {
      id: 'project',
      label: 'Project',
      required: true,
      editHtml:
        '                            <div class="field-group" >\n                                                                        <label for="project">Project<span class="aui-icon icon-required">Required</span></label>\n                <input data-container-class="project-ss" type="text" class="project-field" value="43104"\n       id="project" name="pid"/>\n<div id="project-options" data-suggestions="[{&quot;label&quot;:&quot;Recent Projects&quot;,&quot;items&quot;:[{&quot;label&quot;:&quot;Non-Billable Activities (NB)&quot;,&quot;value&quot;:&quot;15104&quot;,&quot;icon&quot;:&quot;https://jira.ontrq.com/secure/projectavatar?size=xsmall&amp;pid=15104&amp;avatarId=10011&quot;,&quot;selected&quot;:false},{&quot;label&quot;:&quot;INTERNAL PORTAL (IP)&quot;,&quot;value&quot;:&quot;12900&quot;,&quot;icon&quot;:&quot;https://jira.ontrq.com/secure/projectavatar?size=xsmall&amp;pid=12900&amp;avatarId=10011&quot;,&quot;selected&quot;:false}]},{&quot;label&quot;:&quot;All Projects&quot;,&quot;items&quot;:[{&quot;label&quot;:&quot;Absences (ABS)&quot;,&quot;value&quot;:&quot;15105&quot;,&quot;icon&quot;:&quot;https://jira.ontrq.com/secure/projectavatar?size=xsmall&amp;pid=15105&amp;avatarId=10011&quot;,&quot;selected&quot;:false},{&quot;label&quot;:&quot;Astound Commerce Demandware Community (ACDC)&quot;,&quot;value&quot;:&quot;21601&quot;,&quot;icon&quot;:&quot;https://jira.ontrq.com/secure/projectavatar?size=xsmall&amp;pid=21601&amp;avatarId=23407&quot;,&quot;selected&quot;:false},{&quot;label&quot;:&quot;Astound CRM Development and Support (SFDA)&quot;,&quot;value&quot;:&quot;24202&quot;,&quot;icon&quot;:&quot;https://jira.ontrq.com/secure/projectavatar?size=xsmall&amp;pid=24202&amp;avatarId=10011&quot;,&quot;selected&quot;:false},{&quot;label&quot;:&quot;Astound People (FS)&quot;,&quot;value&quot;:&quot;25500&quot;,&quot;icon&quot;:&quot;https://jira.ontrq.com/secure/projectavatar?size=xsmall&amp;pid=25500&amp;avatarId=10011&quot;,&quot;selected&quot;:false},{&quot;label&quot;:&quot;Business Process Architecture (BPA)&quot;,&quot;value&quot;:&quot;36700&quot;,&quot;icon&quot;:&quot;https://jira.ontrq.com/secure/projectavatar?size=xsmall&amp;pid=36700&amp;avatarId=10010&quot;,&quot;selected&quot;:false},{&quot;label&quot;:&quot;Creative Team Project (CTP)&quot;,&quot;value&quot;:&quot;23802&quot;,&quot;icon&quot;:&quot;https://jira.ontrq.com/secure/projectavatar?size=xsmall&amp;pid=23802&amp;avatarId=28401&quot;,&quot;selected&quot;:false},{&quot;label&quot;:&quot;Delivery Marketing Department (MD)&quot;,&quot;value&quot;:&quot;12200&quot;,&quot;icon&quot;:&quot;https://jira.ontrq.com/secure/projectavatar?size=xsmall&amp;pid=12200&amp;avatarId=20604&quot;,&quot;selected&quot;:false},{&quot;label&quot;:&quot;FINANCE AND LEGAL (UA) (FINDEPTCP)&quot;,&quot;value&quot;:&quot;16700&quot;,&quot;icon&quot;:&quot;https://jira.ontrq.com/secure/projectavatar?size=xsmall&amp;pid=16700&amp;avatarId=10011&quot;,&quot;selected&quot;:false}]}]"></div>\n                                    </div>\n    ',
    },
  ],
};

export const result = [
  {
    label: 'Absences (ABS)',
    value: '15105',
    icon:
      'https://jira.ontrq.com/secure/projectavatar?size=xsmall&pid=15105&avatarId=10011',
    selected: false,
  },
  {
    label: 'Astound Commerce Demandware Community (ACDC)',
    value: '21601',
    icon:
      'https://jira.ontrq.com/secure/projectavatar?size=xsmall&pid=21601&avatarId=23407',
    selected: false,
  },
  {
    label: 'Astound CRM Development and Support (SFDA)',
    value: '24202',
    icon:
      'https://jira.ontrq.com/secure/projectavatar?size=xsmall&pid=24202&avatarId=10011',
    selected: false,
  },
  {
    label: 'Astound People (FS)',
    value: '25500',
    icon:
      'https://jira.ontrq.com/secure/projectavatar?size=xsmall&pid=25500&avatarId=10011',
    selected: false,
  },
  {
    label: 'Business Process Architecture (BPA)',
    value: '36700',
    icon:
      'https://jira.ontrq.com/secure/projectavatar?size=xsmall&pid=36700&avatarId=10010',
    selected: false,
  },
  {
    label: 'Creative Team Project (CTP)',
    value: '23802',
    icon:
      'https://jira.ontrq.com/secure/projectavatar?size=xsmall&pid=23802&avatarId=28401',
    selected: false,
  },
  {
    label: 'Delivery Marketing Department (MD)',
    value: '12200',
    icon:
      'https://jira.ontrq.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=20604',
    selected: false,
  },
  {
    label: 'FINANCE AND LEGAL (UA) (FINDEPTCP)',
    value: '16700',
    icon:
      'https://jira.ontrq.com/secure/projectavatar?size=xsmall&pid=16700&avatarId=10011',
    selected: false,
  },
];
