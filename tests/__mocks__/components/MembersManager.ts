export const renderDetaList = jest.fn();
export const showLoader = jest.fn();
export const hideLoader = jest.fn();
export const members = [];
export const elements = {
  searchInput: 'searchInput',
  memberList: 'memberList',
};

export const mockMembersManager = {
  container: null,
  project: null,
  enabledMembers: null,
  members,
  elements,
  renderDetaList,
  showLoader,
  hideLoader,
};

const mock = jest
  .fn()
  .mockImplementation((container, project, enabledMembers) => {
    mockMembersManager.container = container;
    mockMembersManager.project = project;
    mockMembersManager.enabledMembers = enabledMembers;
    return mockMembersManager;
  });

export default mock;
