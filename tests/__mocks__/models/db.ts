export const getAllFromIndex = jest.fn(() => new Promise(resolve => resolve()));
export const add = jest.fn(() => new Promise(resolve => resolve()));
export const get = jest.fn(() => new Promise(resolve => resolve()));
export const _delete = jest.fn(() => new Promise(resolve => resolve()));

const mockDB = {
  getAllFromIndex,
  delete: _delete,
  add,
  get,
};

export default new Promise((resolve, reject) => {
  resolve(mockDB);
});
