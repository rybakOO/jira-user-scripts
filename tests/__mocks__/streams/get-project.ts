import { constant } from 'kefir';

const projects = jest.fn(() =>
  constant([
    {
      label: 'label',
      value: 'value',
      icon: 'icon',
    },
  ])
);

export default projects;
