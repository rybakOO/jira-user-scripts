import { constant } from 'kefir';
export const handleEventElement = jest
  .fn()
  .mockImplementation(() => constant(null));

export const getAttribute = jest.fn().mockImplementation(() => constant(null));

export const watchForExistDOMElement = jest.fn();

export const getElement = jest.fn();
