import { constant } from 'kefir';

const mock = jest.fn().mockImplementation(() =>
  constant({
    type: 'loading',
  })
);

export default mock;
