import { constant } from 'kefir';

const projectId = jest.fn(() => constant('000000'));

export default projectId;
