import members from '../../data/members';

export default {
  getMembersByProjectId: jest.fn(() => Promise.resolve(members)),
};
