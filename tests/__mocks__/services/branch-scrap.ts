import { constant } from 'kefir';

export default jest.fn().mockImplementation(() => constant('feature/NV-12345'));
