import { constant } from 'kefir';

export default jest
  .fn()
  .mockImplementation(() => constant('NV-12345:ADD: task name'));
