module.exports = [
  {
    "self":"https://jira.ontrq.com/rest/api/2/user?username=v.pupkin-second",
    "avatarUrls":{
      "32x32":"https://cdn1.iconfinder.com/data/icons/user-pictures/100/male3-512.png"
    },
    "key":"v.pupkin-second",
    "name":"v.pupkin-second",
    "displayName":"Vasia Pupkin Second",
    "active": true,
    "timeZone":"America/Los_Angeles",
    "locale":"en_US"
  },
  {
    "self":"https://jira.ontrq.com/rest/api/2/user?username=v.pupkin-third",
    "avatarUrls":{
      "32x32":"https://cdn1.iconfinder.com/data/icons/user-pictures/100/male3-512.png"
    },
    "key":"v.pupkin-third",
    "name":"v.pupkin-third",
    "displayName":"Vasia Pupkin Third",
    "active": true,
    "timeZone":"America/Los_Angeles",
    "locale":"en_US"
  },
  {
    "self":"https://jira.ontrq.com/rest/api/2/user?username=v.pupkin-fourth",
    "avatarUrls":{
      "32x32":"https://cdn1.iconfinder.com/data/icons/user-pictures/100/male3-512.png"
    },
    "key":"v.pupkin-fourth",
    "name":"v.pupkin-fourth",
    "displayName":"Vasia Pupkin Fourth",
    "active": true,
    "timeZone":"America/Los_Angeles",
    "locale":"en_US"
  },
  {
    "self":"https://jira.ontrq.com/rest/api/2/user?username=v.pupkin-fifth",
    "avatarUrls":{
      "32x32":"https://cdn1.iconfinder.com/data/icons/user-pictures/100/male3-512.png"
    },
    "key":"v.pupkin-fifth",
    "name":"v.pupkin-fifth",
    "displayName":"Vasia Pupkin Fifth",
    "active": true,
    "timeZone":"America/Los_Angeles",
    "locale":"en_US"
  },
]