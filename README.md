## Installing
`npm install` or `yarn install`

## Developing
`npm run start-dev` or `yarn start-dev`

After let's open `http://localhost:8080`

## Building production bundle
`npm run start-prod` or `yarn start-prod`

## Run tests
`npm run test` or `yarn test`