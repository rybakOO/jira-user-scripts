const tsc = require('typescript');
const tsConfig = require('./../tsconfig.json');

// For jest.json
// "moduleNameMapper": {
//   "^services/(.*)$": "/src/services/$1",
//   "^components/(.*)$": "/src/components/$1",
//   "^interfaces/(.*)$": "/src/interfaces/$1",
//   "^streams/(.*)$": "/src/streams/$1",
//   "^templates/(.*)$": "/src/templates/$1",
//   "^utils/(.*)$": "/src/utils/$1"
// },

module.exports = {
  process(src, path) {
    const isTs = path.endsWith('.ts');

    if (isTs) {
      return tsc.transpileModule(src, {
        compilerOptions: tsConfig.compilerOptions,
        fileName: path,
      }).outputText;
    }

    return src;
  },
};
