module.exports = `
// ==UserScript==
// @name        JIRA User Scripts
// @namespace   https://jira.ontrq.com/
// @include     https://jira.ontrq.com/browse/*
// @include     https://jira.ontrq.com/projects/*
// @version      1
// @grant        none
// ==/UserScript==
`;
