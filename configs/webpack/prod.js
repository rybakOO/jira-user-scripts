// Production config
const merge = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const { resolve } = require('path');
const headline = require('../headline');

const commonConfig = require('./common');

module.exports = merge(commonConfig, {
  mode: 'production',
  entry: './index.ts',
  output: {
    filename: 'js/bundle.min.js',
    path: resolve(__dirname, '../../dist'),
    publicPath: '/',
  },
  plugins: [],
  optimization: {
    minimizer: [
      new TerserPlugin({
        sourceMap: false,
        extractComments: false,
        terserOptions: {
          output: {
            beautify: false,
            preamble: headline,
          },
        },
      }),
    ],
  },
});
