// development config
const merge = require('webpack-merge');
const webpack = require('webpack');
const commonConfig = require('./common');
const {resolve} = require('path');
const assignees = require('../../data/assignees');
const projects = require('../../data/projects');

module.exports = merge(commonConfig, {
  mode: 'development',
  entry: [
    'webpack-dev-server/client?http://localhost:8080',// bundle the client for webpack-dev-server and connect to the provided endpoint
    'webpack/hot/only-dev-server', // bundle the client for hot reloading, only- means to only hot reload for successful updates
    './index.ts' // the entry point of our app
  ],
  output: {
    path: resolve(__dirname, '../../dist'), // Folder to store generated bundle
    filename: 'bundle.js',  // Name of generated bundle after build
    publicPath: '/' // public URL of the output directory when referenced in a browser
  },
  devtool: 'cheap-module-eval-source-map',
  devServer: {
    index: 'index.html',
    setup: function(app, server) {
      app.get('/rest/api/latest/user/assignable/search', function(req, res) {
        res.json(assignees);
      });
      
      app.get('/secure/QuickCreateIssue!default.jspa', function(req, res) {
        res.json(projects);
      });
    }
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(), // enable HMR globally
    new webpack.NamedModulesPlugin(), // prints more readable module names in the browser console on HMR updates
  ],
});
