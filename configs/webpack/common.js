// shared config (dev and prod)
const { resolve } = require('path');
const { CheckerPlugin } = require('awesome-typescript-loader');
const { BannerPlugin } = require('webpack');
const headline = require('../headline');

module.exports = {
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      services: resolve(__dirname, '../../src/services/'),
      components: resolve(__dirname, '../../src/components/'),
      constants: resolve(__dirname, '../../src/constants/'),
      interfaces: resolve(__dirname, '../../src/interfaces/'),
      models: resolve(__dirname, '../../src/models/'),
      streams: resolve(__dirname, '../../src/streams/'),
      templates: resolve(__dirname, '../../src/templates/'),
      utils: resolve(__dirname, '../../src/utils/'),
    },
  },
  context: resolve(__dirname, '../../src'),
  module: {
    rules: [
      {
        test: /\.js$/,
        use: ['babel-loader', 'source-map-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.tsx?$/,
        use: ['babel-loader', 'awesome-typescript-loader'],
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          { loader: 'css-loader', options: { importLoaders: 1 } },
        ],
      },
      {
        test: /\.(scss|sass)$/,
        loaders: [
          'style-loader',
          { loader: 'css-loader', options: { importLoaders: 1 } },
          'sass-loader',
        ],
      },
    ],
  },
  plugins: [
    new CheckerPlugin(),
    new BannerPlugin({
      banner: headline,
      raw: true,
      entryOnly: true,
    }),
  ],
  performance: {
    hints: false,
  },
};
