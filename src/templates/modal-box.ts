/**
 * @param id - Attr id of a element
 * @param iconClass - set of classes for displaying an icon of button
 * @param title
 */
export default `
  <li>
    <a id="{{ id }}" role="button" href="#{{ id }}-box" title="{{ title }}">
      <span class="{{ iconClass }}"></span>
    </a>
  </li>
`;
