/**
 * @param content - content of a tooltip
 * @param iconClass - set of classes for displaying an icon of button
 */
export default `
  <button id="{{ id }}" class="aui-button aui-button-primary" title="{{ content }}">
    <span class="{{ iconClass }}"></span>
    <span class="trigger-label"></span>
  </button>
`;