/**
 * @param id - Attr id of the element
 * @param members
 */
export default `
  <ul id="{{ id }}" class="members-manager--list members-manager--list__selected" data-testid="member-list">
    {{if(Array.isArray(options.members) && options.members.length )}}
      {{each(options.members)}}
        <li class="user" data-testid="member-list-{{ @this.key }}">
          <img class="user--image" src="{{ @this.avatarUrls['32x32'] }}" alt="{{ @this.name }}" title="{{ @this.displayName }}"/>
          <span class="user--name">{{ @this.displayName }}</span>
          <buttun type="button" class="user--ctrl user--add aui-button aui-button-primary" data-member-key="{{ @this.key }}">
            <i class="aui-icon aui-icon-small aui-iconfont-remove"></i>
          </buttun>
        </li>
      {{/each}}
    {{#else}}
      <li>
        List member is empty!
      </li>
    {{/if}}
  </ul>
`;
