import button from './button';
import modalBox from './modal-box';
import membersManagerForm from './members-manager-form';
import membersManagerDatalist from './members-manager-datalist';
import membersManagerMemberList from './members-manager-member-list';
import assigneeList from './assignee-list';
import settings from './settings';

export default {
  assigneeList,
  button,
  modalBox,
  membersManagerForm,
  membersManagerDatalist,
  membersManagerMemberList,
  settings
}