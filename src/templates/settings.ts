/**
 * @param id - Attr id of the element
 * @param prefixBranchBug string
 * @param prefixBranchFeature string
 */
 export default `
 <div id="{{ id }}" class="settings">
  <form>
    <div class="g-flex">
      <div class="g-flex--50">
        <div class="settings--input">
          <label class="toggle-title">
            <div>Prefix branch for bug type:</div>
            <input class="text long-field user-scripts-input" name="prefix-branch-bugs" placeholder="type prefix for bugs" value="{{ prefixBranchBugs }}"/>
          </label>
        </div>
      </div>
      <div class="g-flex--50">
        <div class="settings--input">
          <label class="toggle-title">
            <div>Prefix branch for feature type:</div>
            <input class="text long-field user-scripts-input" name="prefix-branch-features" placeholder="type prefix for features" value="{{ prefixBranchFeatures }}"/>
          </label>
        </div>
      </div>
      <div class="settings--wrap-submit g-flex--30">
        <button class="aui-button aui-button-primary" title="Submit">
          Save
        </button>
      </div>
    </div>
   <form>
 </div>
`;
