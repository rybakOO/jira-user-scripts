/**
 * @param id - Attr id of the element
 * @param project IProject
 */
export default `
  <div id="{{ id }}" class="members-manager">
    <div class="g-flex">
      <div class="g-flex--50">
        {{if(options.project)}}
          <div class="members-manager--input" data-testid="project">
            <div class="project">
              <span class="project--title toggle-title">
                Project:
              </span>
              <img class="project--image" src="{{ project.icon }}" data-testid="project-image"/>
              <b class="project--label">{{ project.label }}</b>
            </div>
          </div>
        {{/if}}
        <div class="members-manager--input">
          <label class="toggle-title">
            <div>Members:</div>
            <input class="text long-field user-scripts-input" name="members-manager" placeholder="search..."/>
            <div class="loader loader__small"></div>
          </label>
        </div>
        <div id="members-manager-wrap-datalist"></div>
      </div>
      <div class="g-flex--50">
        <div id="members-manager-selected" class="members-manager--selected">
          <h3>Selected members</h3>
        </div>
      </div>
    </div>
  </div>
`;
