/**
 * @param id - Attr id of the element
 * @param members
 */
export default `
  <ul id="{{ id }}" class="members-manager--list">
    {{if(Array.isArray(options.members))}}
      {{each(options.members)}}
        <li class="user">
          <img class="user--image" src="{{ @this.avatarUrls['32x32'] }}" alt="{{ @this.name }}" title="{{ @this.displayName }}"/>
          <span class="user--name">{{ @this.displayName }}</span>
          {{if(!options.enabledMemberKeys.includes(@this.key))}}
            <buttun type="button" class="user--ctrl user--add aui-button aui-button-primary" data-member-key="{{ @this.key }}">
              <i class="aui-icon aui-icon-small aui-iconfont-add"></i>
            </buttun>
          {{/if}}
        </li>
      {{/each}}
    {{#else}}
      {{if(options.members.error)}}
        <li>
          Something wrong!
        </li>
      {{#else}}
        <li>
          Nothing found!
        </li>
      {{/if}}
    {{/if}}
  </ul>
`;