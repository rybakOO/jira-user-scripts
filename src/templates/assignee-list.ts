/**
 * @param id - Attr id of the element
 * @param members
 * @param taskId
 * @param token
 */
export default `
<div id="{{ id }}" class="assignee-list" data-testid="assignee-list">
    {{if(Array.isArray(options.members))}}
      {{each(options.members)}}
        <span>
          <a href="/secure/AssignIssue.jspa?atl_token={{ token }}&amp;id={{ taskId }}&amp;assignee={{ @this.key }}" title="Assign this issue to {{ @this.displayName }}" data-testid="assignee-list-{{ @this.key }}">
            Assign to {{ @this.displayName }}
          </a>
        </span>
      {{/each}}
    {{/if}}
  </div>
`;
