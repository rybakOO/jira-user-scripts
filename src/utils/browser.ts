export const getPathname = () =>
  `${location.protocol}//${location.hostname}${
    location.port ? `${location.port}` : ''
  }`;
