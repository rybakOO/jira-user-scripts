export const getTextContentBySelector = (
  selector: string,
  container: HTMLElement | Document = document
): string => {
  const element = container.querySelector(selector);
  if (element) return element.textContent.trim();
  return '';
};
