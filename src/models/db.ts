import { openDB, DBSchema } from 'idb';

const DATABASE_VERSION = 1;

interface MainDBSchema extends DBSchema {
  members: {
    value: {
      projectId: string;
      key: string;
      displayName: string;
      name: string;
      self: string;
      avatarUrls: {
        '32x32': string;
      };
    };
    key: string;
    indexes: { 'by-product-id': string };
  };
  settings: {
    value: {
      projectId: string;
      prefixBranchBugs: string;
      prefixBranchFeatures: string;
    };
    key: string;
    indexes: { 'by-product-id': string };
  };
}

async function bootstrapDB() {
  const db = await openDB<MainDBSchema>('jira-user-scripts', DATABASE_VERSION, {
    upgrade(db) {
      const members = db.createObjectStore('members');
      const settings = db.createObjectStore('settings');
      members.createIndex('by-product-id', 'projectId');
      settings.createIndex('by-product-id', 'projectId');
    },
  });

  // This works
  // await db.put('favourite-number', 7, 'Jen');
  // This fails at compile time, as the 'favourite-number' store expects a number.
  // await db.put('favourite-number', 'Twelve', 'Jake');
  return db;
}

export default bootstrapDB();
