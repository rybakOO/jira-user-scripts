import './assets/scss/index.scss';
import '../typings/jira';
import { combine, constant, zip, concat } from 'kefir';
import { getElement } from './streams/dom';
import getCopyTargetListener from './streams/copy-target';
import pageReady from './streams/page-ready';
import refreshPage$ from './streams/refresh-page';
import createModalBox from './streams/modal-box';
import initAssineeSettings from './streams/assignee-settings';
import initSettings from './streams/settings';
import getProjects from './streams/get-project';
import copyBuffer from './services/copy-buffer';
import { HEADER_MAIN_SELECTOR, NAV_BAR_SELECTOR } from './constants/selectors';
import {
  SETTING,
  MEMBER_ACTION_REMOVE,
  MEMBER_ACTION_ADD,
} from './constants/types';
import taskId from './streams/task-id';
import taskKey from './streams/task-key';
import projectId from './streams/project-id';
import token$ from './streams/token';
import currentProjectMembersUpdated$ from './streams/current-project-members-updated';
import projectsStore from './services/members-store';
import getCurrentProject from './streams/current-project';
import AssigneeList from './components/AssigneeList';
import settingsUpdated$ from 'streams/settings-updated';
import getCommitDescription from 'services/commit-scrap';
import branchDescription$ from 'services/branch-scrap';

const copyTargetClick = () => 
  combine(
    [zip([constant(getCommitDescription()), branchDescription$]), getElement(HEADER_MAIN_SELECTOR)],
    getCopyTargetListener
  )
  .flatMap(stream$ => stream$);


const bootstrap = () => {
  const pageReady$ = pageReady();
  const projects$ = getProjects();

  pageReady$
    .flatMap(copyTargetClick)
    .onValue(text => copyBuffer(text));

  const openModal$ = pageReady$
    .flatMap(() =>
      combine([
        createModalBox(
          document.querySelector(NAV_BAR_SELECTOR),
          SETTING,
          'modal-nav-bar'
        ),
        currentProjectMembersUpdated$,
      ])
    )
    .flatMap(([container, enabledMembers]) => 
      zip([
        constant(container),
        getCurrentProject(projects$, projectId()),
        taskKey(),
        constant(enabledMembers).toProperty(),
      ])
    );

  openModal$.flatMap(initAssineeSettings)
    .onValue(({ type, projectId, member }) => {
      if (type === MEMBER_ACTION_ADD) {
        projectsStore.addItem(projectId, member);
      } else if (type === MEMBER_ACTION_REMOVE) {
        projectsStore.removeItem(projectId, member);
      }
    });

  combine([openModal$, settingsUpdated$])
    .flatMap(initSettings)
    .onValue(({projectId, settings}) => {
      projectsStore.updateSettings(projectId, settings);
    })

  pageReady$
    .flatMap(() => currentProjectMembersUpdated$)
    .flatMap(enabledMembers => {
      return zip([constant(enabledMembers).toProperty(), taskId(), token$]);
    })
    .onValue(([members, issueKey, token]) => {
      AssigneeList(members, issueKey, token);
    });

  return pageReady$;
};

bootstrap();

refreshPage$.flatMap(bootstrap).observe();
