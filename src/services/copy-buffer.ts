const inputHidden = document.createElement('input');
inputHidden.type = 'text';
inputHidden.style.display = 'none';
document.body.appendChild(inputHidden);

export default (text: string) => {
  inputHidden.style.display = 'block';
  inputHidden.value = text;
  inputHidden.select();
  document.execCommand('copy');
  inputHidden.style.display = 'none';
};
