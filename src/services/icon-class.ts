import { BRANCH, COMMIT, SETTING } from 'constants/types';

const BASE_CLASSES = `aui-icon aui-icon-small`;

const getIconClass = (type: string): string => {
  let classes: string = '';
  switch (type) {
    case BRANCH:
      classes = `${BASE_CLASSES} aui-iconfont-share`;
      break;
    case COMMIT:
      classes = `${BASE_CLASSES} aui-iconfont-email`;
      break;
    case SETTING:
      classes = `${BASE_CLASSES} aui-iconfont-settings`;
      break;
  }

  return classes;
};

export default getIconClass;
