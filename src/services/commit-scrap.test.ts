import commitScrap, { defineCommitType } from './commit-scrap';
import { getTextContentBySelector } from 'utils/dom';
import {
  TASK_KEY_SELECTOR,
  TASK_TYPE_SELECTOR,
  TASK_SUMMARY_SELECTOR,
} from 'constants/selectors';
import { COMMIT_TYPE_BUG, COMMIT_TYPE_FEATURE } from 'constants/types';

const taskId = 'FF-123456';
const taskType = 'task';
const taskSummary = 'summary';

const consoleWarn = (global.console.warn = jest.fn());

// @ts-ignore
getTextContentBySelector.mockImplementation(value => {
  if (TASK_KEY_SELECTOR === value) {
    return taskId;
  } else if (TASK_TYPE_SELECTOR === value) {
    return taskType;
  } else if (TASK_SUMMARY_SELECTOR === value) {
    return taskSummary;
  }
});

it('Commit scrap service works properly', () => {
  let result = commitScrap();

  expect(getTextContentBySelector).toHaveBeenNthCalledWith(
    1,
    TASK_KEY_SELECTOR
  );
  expect(getTextContentBySelector).toHaveBeenNthCalledWith(
    2,
    TASK_TYPE_SELECTOR
  );
  expect(getTextContentBySelector).toHaveBeenNthCalledWith(
    3,
    TASK_SUMMARY_SELECTOR
  );
  expect(result).toMatchObject({
    type: 'COMMIT',
    content: `${taskId}:${defineCommitType(taskType)}: ${taskSummary}`,
  });

  // @ts-ignore
  getTextContentBySelector.mockClear();
  // @ts-ignore
  getTextContentBySelector.mockImplementation(value => {
    if (TASK_KEY_SELECTOR === value) {
      return taskId;
    } else if (TASK_TYPE_SELECTOR === value) {
      return taskType;
    } else if (TASK_SUMMARY_SELECTOR === value) {
      return '';
    }
  });

  result = commitScrap();

  expect(consoleWarn).toBeCalled();
  expect(result).toMatchObject({
    type: 'COMMIT',
    content: `${taskId}:${defineCommitType(taskType)}: `,
  });

  // @ts-ignore
  getTextContentBySelector.mockClear();
  // @ts-ignore
  // @ts-ignore
  getTextContentBySelector.mockImplementation(value => {
    if (TASK_KEY_SELECTOR === value) {
      return taskId;
    } else if (TASK_TYPE_SELECTOR === value) {
      return 'BUG';
    } else if (TASK_SUMMARY_SELECTOR === value) {
      return taskSummary;
    }
  });

  result = commitScrap();

  expect(result).toMatchObject({
    type: 'COMMIT',
    content: `${taskId}:${defineCommitType('BUG')}: ${taskSummary}`,
  });

  expect(defineCommitType('Bug')).toBe(COMMIT_TYPE_BUG);
  expect(defineCommitType('Something')).toBe(COMMIT_TYPE_FEATURE);
});
