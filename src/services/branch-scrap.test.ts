import branchScrap$, { defineBranchType } from './branch-scrap';
import { getTextContentBySelector } from 'utils/dom';
import { TASK_KEY_SELECTOR, TASK_TYPE_SELECTOR } from 'constants/selectors';
import { BRANCH_TYPE_BUG, BRANCH_TYPE_FEATURE } from 'constants/types';
import ISettings from 'interfaces/settings';

const key = 'FF-123456';
const type = 'task';

const consoleWarn = (global.console.warn = jest.fn());

const settings: ISettings = {
  prefixBranchBugs: 'bugfix',
  prefixBranchFeatures: 'feature',
};

// @ts-ignore
getTextContentBySelector.mockImplementation(value => {
  if (TASK_KEY_SELECTOR === value) {
    return key;
  } else if (TASK_TYPE_SELECTOR === value) {
    return type;
  }
});

it('Branch scrap service works properly', () => {
    branchScrap$.onValue((result) => {

    expect(getTextContentBySelector).toHaveBeenNthCalledWith(
      1,
      TASK_KEY_SELECTOR
    );
    expect(getTextContentBySelector).toHaveBeenNthCalledWith(
      2,
      TASK_TYPE_SELECTOR
    );
    expect(result).toMatchObject({
      type: 'BRANCH',
      content: `feature/${key}`,
    });
  
    // @ts-ignore
    getTextContentBySelector.mockClear();
    // @ts-ignore
    getTextContentBySelector.mockImplementation(value => {
      if (TASK_KEY_SELECTOR === value) {
        return '';
      } else if (TASK_TYPE_SELECTOR === value) {
        return type;
      }
    });
  });

  branchScrap$.onValue((result) => {
    expect(consoleWarn).toBeCalled();
    expect(result).toMatchObject({
      type: 'BRANCH',
      content: `feature/`,
    });

    // @ts-ignore
    getTextContentBySelector.mockClear();
    // @ts-ignore
    getTextContentBySelector.mockImplementation(value => {
      if (TASK_KEY_SELECTOR === value) {
        return key;
      } else if (TASK_TYPE_SELECTOR === value) {
        return 'bug';
      }
    });
  });

  branchScrap$.onValue((result) => {
    expect(result).toMatchObject({
      type: 'BRANCH',
      content: `bugfix/${key}`,
    });
  
    expect(defineBranchType('Bug', settings)).toBe(BRANCH_TYPE_BUG);
    expect(defineBranchType('Something', settings)).toBe(BRANCH_TYPE_FEATURE);
  });
});
