import '@testing-library/jest-dom';
import * as Sqrl from 'squirrelly';
import { insertElementById } from './dom';
import templates from 'templates';

const id = 'child';
const params = {
  property: 'property',
  property1: 'property1',
};
const templateName = 'templateName';
const container = document.createElement('div');
const position = 'beforeend';
document.body.append(container);

Sqrl.Render.mockImplementation(() => `<span id="${id}">Lorem<span>`);

describe('DOM service works properly', () => {
  test('insertElementById works correct', () => {
    const element = insertElementById(
      id,
      params,
      templateName,
      container,
      position
    );

    expect(element).toBeInstanceOf(HTMLSpanElement);
    expect(element.textContent).toContain('Lorem');
    expect(Sqrl.Render).toHaveBeenCalledWith(templates[templateName], {
      id,
      ...params,
    });

    const elementNext = insertElementById(
      id,
      params,
      templateName,
      container,
      position
    );

    expect(container).not.toContainElement(element);
    expect(container).toContainElement(elementNext);
  });
});
