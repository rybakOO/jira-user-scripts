import copyBuffer from './copy-buffer';

const execCommand = (document.execCommand = jest.fn());
const text = 'Copy buffer service works properly';

it('Copy buffer service works properly', () => {
  copyBuffer(text);
  const input = document.body.querySelector('input');
  expect(input).toBeInstanceOf(HTMLInputElement);
  expect(input.value).toContain(text);
  expect(execCommand).toHaveBeenCalled();
  expect(input.style.display).toContain('none');
});
