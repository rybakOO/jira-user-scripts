import * as Sqrl from 'squirrelly';
import templates from 'templates';

export const insertElementById = (
  id: string,
  params: object,
  templateName: string,
  container: HTMLElement,
  position: InsertPosition = 'beforeend'
): HTMLElement => {
  const template = Sqrl.Render(templates[templateName], {
    id,
    ...params,
  });
  const existElement = container.querySelector(`#${id}`);
  if (existElement) {
    existElement.remove();
  }
  container.insertAdjacentHTML(position, template);
  return container.querySelector(`#${id}`) as HTMLElement;
};
