import iconClass from './icon-class';

it('Icon class service works properly', () => {
  expect(iconClass('BRANCH')).toContain('aui-iconfont-share');
  expect(iconClass('COMMIT')).toContain('aui-iconfont-email');
  expect(iconClass('SETTING')).toContain('aui-iconfont-settings');
});
