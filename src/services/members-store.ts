import { PROJECT_UPDATE_EVENT } from 'constants/types';
import IMember from 'interfaces/member';
import ISettings from 'interfaces/settings';
import gatDatabase from 'models/db';

const event = new Event(PROJECT_UPDATE_EVENT);

export default {
  async getMembersByProjectId(projectId: string) {
    const db = await gatDatabase;

    return await db.getAllFromIndex('members', 'by-product-id', projectId);
  },

  async getSettingsByProjectId(projectId: string) {
    const db = await gatDatabase;

    return await db.getAllFromIndex('settings', 'by-product-id', projectId);
  },

  async updateSettings(projectId: string, settings: ISettings) {
    const db = await gatDatabase;

    await db.put(
      'settings',
      {
        projectId,
        prefixBranchBugs: settings.prefixBranchBugs,
        prefixBranchFeatures: settings.prefixBranchFeatures,
      },
      projectId
    );

    window.dispatchEvent(event);
  },

  async addItem(projectId: string, member: IMember) {
    const db = await gatDatabase;
    const isMemberExisted = await db.get(
      'members',
      `${projectId}-${member.key}`
    );

    if (isMemberExisted) {
      return;
    }

    db.add(
      'members',
      {
        projectId,
        key: member.key,
        displayName: member.displayName,
        name: member.name,
        self: member.self,
        avatarUrls: member.avatarUrls,
      },
      `${projectId}-${member.key}`
    );

    window.dispatchEvent(event);
  },

  async removeItem(projectId: string, member: IMember) {
    const db = await gatDatabase;
    db.delete('members', `${projectId}-${member.key}`);
    window.dispatchEvent(event);
  },
};
