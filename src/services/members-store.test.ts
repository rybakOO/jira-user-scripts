import membersStore from './members-store';
// @ts-ignore
import { getAllFromIndex, add, _delete, get } from 'models/db';
import members from '../../tests/data/members';

const projectId = '000000';
const member = members.shift();

const dispatchEvent = (window.dispatchEvent = jest.fn());

describe('Members store service works properly', () => {
  beforeEach(() => {
    dispatchEvent.mockClear();
  });

  test('getMembersByProjectId works correct', async () => {
    await membersStore.getMembersByProjectId(projectId);

    expect(getAllFromIndex).toBeCalledWith(
      'members',
      'by-product-id',
      projectId
    );
  });

  test('addItem works correct', async () => {
    await membersStore.addItem(projectId, member);

    expect(get).toBeCalledWith('members', `${projectId}-${member.key}`);

    expect(add).toBeCalledWith(
      'members',
      {
        projectId,
        key: member.key,
        displayName: member.displayName,
        name: member.name,
        self: member.self,
        avatarUrls: member.avatarUrls,
      },
      `${projectId}-${member.key}`
    );
    expect(dispatchEvent).toHaveBeenCalled();

    // @ts-ignore
    add.mockClear();
    dispatchEvent.mockClear();
    get.mockImplementation(() => member);

    await membersStore.addItem(projectId, member);

    expect(add).not.toHaveBeenCalled();
    expect(dispatchEvent).not.toHaveBeenCalled();
  });

  test('removeItem works correct', async () => {
    await membersStore.removeItem(projectId, member);

    dispatchEvent.mockClear();

    expect(_delete).toBeCalledWith('members', `${projectId}-${member.key}`);
    expect(dispatchEvent).not.toHaveBeenCalled();
  });
});
