import { TASK_KEY_SELECTOR, TASK_TYPE_SELECTOR } from 'constants/selectors';
import {
  TASK_TYPE_BUG,
  BRANCH_TYPE_BUG,
  BRANCH_TYPE_FEATURE,
} from '../constants/types';
import { getTextContentBySelector } from 'utils/dom';
import { IDescription } from 'interfaces/decription';
import ISettings from '../interfaces/settings';
import { BRANCH } from 'constants/types';
import settingsUpdated$ from 'streams/settings-updated';

const branchDescription$ = settingsUpdated$
    .map((settings: ISettings): IDescription => {
      const taskId = getTextContentBySelector(TASK_KEY_SELECTOR);
      const taskType = getTextContentBySelector(TASK_TYPE_SELECTOR);
      if (!taskId || !taskType) {
        console.warn(
          `[BRANCH-SCRAP]: One of parts of the task description has been not found!`
        );
      }
      return {
        type: BRANCH,
        content: `${defineBranchType(taskType, settings)}/${taskId}`,
      };
    })


export const defineBranchType = (taskType: string, settings: ISettings): string => {
  if (taskType.toUpperCase().includes(TASK_TYPE_BUG)) {
    return settings ? settings.prefixBranchBugs : BRANCH_TYPE_BUG;
  }
  return settings ? settings.prefixBranchFeatures : BRANCH_TYPE_FEATURE;
};

export default branchDescription$;