import {
  TASK_KEY_SELECTOR,
  TASK_SUMMARY_SELECTOR,
  TASK_TYPE_SELECTOR,
} from 'constants/selectors';
import {
  TASK_TYPE_BUG,
  COMMIT_TYPE_BUG,
  COMMIT_TYPE_FEATURE,
} from 'constants/types';
import { getTextContentBySelector } from 'utils/dom';
import { IDescription } from 'interfaces/decription';
import { COMMIT } from 'constants/types';

export default (): IDescription => {
  const taskId = getTextContentBySelector(TASK_KEY_SELECTOR);
  const taskType = getTextContentBySelector(TASK_TYPE_SELECTOR);
  const taskSummary = getTextContentBySelector(TASK_SUMMARY_SELECTOR);
  if (!taskId || !taskType || !taskSummary) {
    console.warn(
      `[COMMIT-SCRAP]: One of parts of the task description has been not found!`
    );
  }
  return {
    type: COMMIT,
    content: `${taskId}:${defineCommitType(taskType)}: ${taskSummary}`,
  };
};

export const defineCommitType = (taskType: string): string => {
  if (taskType.toUpperCase().includes(TASK_TYPE_BUG)) {
    return COMMIT_TYPE_BUG;
  }
  return COMMIT_TYPE_FEATURE;
};
