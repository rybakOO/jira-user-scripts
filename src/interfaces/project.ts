export default interface IProject {
  label: string;
  value: string;
  icon: string;
}
