export interface IDescription {
  type: 'BRANCH' | 'COMMIT';
  content: string;
}
