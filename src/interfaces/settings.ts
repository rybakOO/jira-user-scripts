export default interface ISettings {
  prefixBranchBugs: string; 
  prefixBranchFeatures: string;
}