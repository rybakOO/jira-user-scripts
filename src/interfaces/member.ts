export default interface IMember {
  key: string;
  displayName: string;
  name: string;
  self: string;
  avatarUrls: {
    '32x32': string;
  };
}
