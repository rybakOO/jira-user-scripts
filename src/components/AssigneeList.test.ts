import AssigneeList from './AssigneeList';
import members from '../../tests/data/members';
import { ASSIGNEE_LIST_SELECTOR } from '../constants/selectors';
import { insertElementById } from 'services/dom';

const taskId = '12345';
const token = '0000-0000-0000-0000';

describe('AssigneeList component works properly', () => {
  beforeAll(() => {
    document.body.innerHTML = `
      <ul id="peopledetails">
        <li>
          <dl>
            <dt>Assignee:</dt>
            <dd></dd>
          </dl>
        </li>
      </ul>
    `;
  });

  test('should correctly initialization', () => {
    const container = document.querySelector(ASSIGNEE_LIST_SELECTOR);
    AssigneeList(members, taskId, token);

    expect(insertElementById).toBeCalledWith(
      'assignee-list',
      {
        members,
        taskId,
        token,
      },
      'assigneeList',
      container
    );
  });

  // test('should has correct property', () => {
  //   AssigneeList(members, taskId, token);
  //   getByTestId(document.documentElement, 'assignee-list');
  //   members.forEach(member => {
  //     const el = getByTestId(
  //       document.documentElement,
  //       `assignee-list-${member.key}`
  //     );
  //     expect(el).toBeEnabled();
  //     expect(el).toHaveAttribute(
  //       'href',
  //       expect.stringContaining(
  //         `/secure/AssignIssue.jspa?atl_token=${token}&id=${taskId}&assignee=${member.key}`
  //       )
  //     );
  //     expect(el).toHaveAttribute(
  //       'title',
  //       expect.stringContaining(`Assign this issue to ${member.displayName}`)
  //     );
  //     expect(el).toContainHTML(`Assign this issue to ${member.displayName}`);
  //   });
  // });
});
