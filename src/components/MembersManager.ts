import IProject from 'interfaces/project';
import IMember from 'interfaces/member';
import { insertElementById } from 'services/dom';
import {
  MANAGER_MEMBERS_WRAP_DATALIST_SELECTOR,
  MANAGER_MEMBERS_SELECTED_SELECTOR,
  MANAGER_MEMBERS_INPUT_SELECTOR,
  MANAGER_MEMBERS_ID,
  MANAGER_MEMBERS_DATALIST_ID,
  MANAGER_MEMBERS_LIST_ID,
} from 'constants/selectors';

let instance: MembersManager;

export default class MembersManager {
  private project: IProject;

  private container: HTMLElement;

  private enabledMemberKeys: string[];

  private _members: IMember[];

  public elements: {
    searchInput?: HTMLInputElement;
    memberList?: HTMLElement;
  };

  constructor(
    container: HTMLElement,
    project: IProject,
    enabledMembers: IMember[]
  ) {
    if (instance && document.contains(instance.container)) {
      instance.enabledMemberKeys = enabledMembers.map(member => member.key);
      instance.renderDetaList(instance.members);
      instance.renderMemberList(enabledMembers);
      return instance;
    } else {
      this.container = container;
      this.project = project;
      this.enabledMemberKeys = enabledMembers.map(member => member.key);
      this.members = [];
      this.elements = {};
      container.innerHTML = '';
      this.renderForm();
      this.renderMemberList(enabledMembers);
      instance = this;
    }
  }

  private renderForm() {
    insertElementById(
      MANAGER_MEMBERS_ID,
      {
        project: this.project,
      },
      'membersManagerForm',
      this.container
    );

    this.elements.searchInput = this.container.querySelector(
      MANAGER_MEMBERS_INPUT_SELECTOR
    );
  }

  renderDetaList(members: IMember[] | any) {
    return insertElementById(
      MANAGER_MEMBERS_DATALIST_ID,
      {
        members,
        enabledMemberKeys: this.enabledMemberKeys,
      },
      'membersManagerDatalist',
      this.container.querySelector(
        MANAGER_MEMBERS_WRAP_DATALIST_SELECTOR
      ) as HTMLElement
    );
  }

  renderMemberList(members: IMember[]) {
    return (this.elements.memberList = insertElementById(
      MANAGER_MEMBERS_LIST_ID,
      {
        members,
      },
      'membersManagerMemberList',
      this.container.querySelector(
        MANAGER_MEMBERS_SELECTED_SELECTOR
      ) as HTMLElement
    ));
  }

  showLoader = () => {
    if (!this.elements.searchInput) {
      return;
    }
    const parent = this.elements.searchInput.parentNode as HTMLElement;
    parent.classList.add('members-manager--input__progress');
  };

  hideLoader = () => {
    if (!this.elements.searchInput) {
      return;
    }
    const parent = this.elements.searchInput.parentNode as HTMLElement;
    parent.classList.remove('members-manager--input__progress');
  };

  public get members(): IMember[] {
    return this._members;
  }

  public set members(members: IMember[]) {
    this._members = members;
  }
}
