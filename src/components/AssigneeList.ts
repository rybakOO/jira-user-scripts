import { ASSIGNEE_LIST_SELECTOR } from 'constants/selectors';
import { insertElementById } from 'services/dom';

export default (members, taskId, token) => {
  return insertElementById(
    'assignee-list',
    {
      members,
      taskId,
      token,
    },
    'assigneeList',
    document.querySelector(ASSIGNEE_LIST_SELECTOR)
  );
};
