import { insertElementById } from 'services/dom';
import {
  SETTINGS_ID,
  SETTINGS_FORM_SELECTOR
} from 'constants/selectors';
import ISettings from 'interfaces/settings';

let instance: SettingsManager;

export default class SettingsManager {
  private settings: ISettings;

  private container: HTMLElement;

  public elements: {
    form?: HTMLFormElement;
  };

  constructor(
    container: HTMLElement,
    settings: ISettings,
  ) {
    if (instance && document.contains(instance.container)) {
      instance.settings = settings;
      instance.renderForm();
      return instance;
    } else {
      this.container = container;
      this.settings = settings;
      this.elements= {};
      this.renderForm();
      instance = this;
    }
  }

  private renderForm() {
    insertElementById(
      SETTINGS_ID,
      this.settings,
      'settings',
      this.container
    );

    this.elements.form = this.container.querySelector(
      SETTINGS_FORM_SELECTOR
    );
  }
}
