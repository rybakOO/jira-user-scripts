import MembersManager from './MembersManager';
import { insertElementById } from 'services/dom';
import {
  MANAGER_MEMBERS_ID,
  MANAGER_MEMBERS_DATALIST_ID,
  MANAGER_MEMBERS_LIST_ID,
} from 'constants/selectors';
import members from '../../tests/data/members';

const project = {
  label: 'test project',
  value: 'test project value',
  icon: 'test project icon',
};

let container: HTMLElement;

describe('MembersManager component works properly', () => {
  beforeEach(() => {
    document.body.innerHTML = `
      <div id="container">
        <div id="something"></div>
      </div>
    `;
    container = document.querySelector('#container');
    //@ts-ignore
    insertElementById.mockClear();
  });

  test('should correctly initialization', () => {
    const membersManager = new MembersManager(container, project, members);
    expect(membersManager.elements.searchInput).toBe(null);
    expect(container.querySelector('#something')).toBe(null);
    expect(insertElementById).toHaveBeenNthCalledWith(
      1,
      MANAGER_MEMBERS_ID,
      {
        project,
      },
      'membersManagerForm',
      container
    );
    expect(insertElementById).toHaveBeenNthCalledWith(
      2,
      MANAGER_MEMBERS_LIST_ID,
      {
        members,
      },
      'membersManagerMemberList',
      null
    );
    expect(insertElementById).toHaveBeenCalledTimes(2);
  });

  test('should be singleton during existing container', () => {
    const membersManager = new MembersManager(container, project, members);
    //@ts-ignore
    insertElementById.mockClear();
    const enabledMembers = members.slice(0, 2);
    const membersManagerNext = new MembersManager(
      container,
      project,
      enabledMembers
    );

    expect(membersManager).toBe(membersManagerNext);
    expect(membersManager.elements).toBe(membersManagerNext.elements);
    expect(insertElementById).toHaveBeenNthCalledWith(
      1,
      MANAGER_MEMBERS_DATALIST_ID,
      {
        members: membersManagerNext.members,
        enabledMemberKeys: enabledMembers.map(member => member.key),
      },
      'membersManagerDatalist',
      null
    );
    expect(insertElementById).toHaveBeenNthCalledWith(
      2,
      MANAGER_MEMBERS_LIST_ID,
      {
        members: enabledMembers,
      },
      'membersManagerMemberList',
      null
    );
    expect(insertElementById).toHaveBeenCalledTimes(2);
  });

  test('renderDetaList should work properly', () => {
    const membersManager = new MembersManager(container, project, members);
    const _members = members.slice(2, 4);
    //@ts-ignore
    insertElementById.mockClear();
    membersManager.renderDetaList(_members);
    expect(insertElementById).toHaveBeenCalledWith(
      MANAGER_MEMBERS_DATALIST_ID,
      {
        members: _members,
        enabledMemberKeys: members.map(member => member.key),
      },
      'membersManagerDatalist',
      null
    );
  });

  test('should save members properly', () => {
    const membersManager = new MembersManager(container, project, members);
    //@ts-ignore
    insertElementById.mockClear();
    const _members = members.slice(0, 2);
    membersManager.members = _members;
    const membersManagerNext = new MembersManager(container, project, members);
    expect(insertElementById).toHaveBeenNthCalledWith(
      1,
      MANAGER_MEMBERS_DATALIST_ID,
      {
        members: _members,
        enabledMemberKeys: members.map(member => member.key),
      },
      'membersManagerDatalist',
      null
    );
    expect(insertElementById).toHaveBeenCalledTimes(2);
    expect(membersManagerNext.members).toBe(_members);
  });

  test('Trigger loader should work properly', () => {
    const membersManager = new MembersManager(container, project, members);
    expect(membersManager.elements.searchInput).toBeFalsy();
    container.innerHTML = `
      <div id="wrap-input">
        <input type="text" />
      </div>
    `;
    const wrap = container.querySelector('#wrap-input');
    membersManager.elements.searchInput = container.querySelector(
      '#wrap-input input'
    );
    membersManager.showLoader();
    expect(wrap.className).toContain('members-manager--input__progress');
    membersManager.hideLoader();
    expect(wrap.className).not.toContain('members-manager--input__progress');
  });
});

// function expectMembers(
//   members: IMember[],
//   testId: string,
//   container: HTMLElement
// ) {
//   members.forEach(member => {
//     const el = getByTestId(container, `${testId}-${member.key}`);
//     expect(el).toBeEnabled();
//     expect(el).toContainHTML(member.key);
//     expect(el).toContainHTML(member.name);
//     expect(el).toContainHTML(member.displayName);
//     expect(el).toContainHTML(member.avatarUrls['32x32']);
//   });
// }

// function unexpectMembers(
//   members: IMember[],
//   testId: string,
//   container: HTMLElement
// ) {
//   members.forEach(member => {
//     expect(
//       container.querySelector(`[data-testid="${testId}-${member.key}"]`)
//     ).toBeFalsy();
//   });
// }
