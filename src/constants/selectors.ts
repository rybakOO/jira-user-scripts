export const TASK_KEY_SELECTOR = '#key-val';

export const TASK_ID_SELECTOR = 'input[name="id"]';

export const TASK_SUMMARY_SELECTOR = '#summary-val';

export const TASK_TYPE_SELECTOR = '#type-val';

export const TOKEN_SELECTOR = '#atlassian-token';

export const MAIN_CONTAINER_SELECTOR = '.aui-group.split-view';

export const CONTENT_SELECTOR = '#issue-content';

export const HEADER_MAIN_SELECTOR = '#stalker .aui-page-header-main';

export const NAV_BAR_SELECTOR = '#header .aui-header-secondary > ul';

export const PROJECT_ID_SELECTOR = '[data-project-id]';

export const MANAGER_MEMBERS_ID = 'members-manager';

export const MANAGER_MEMBERS_INPUT_SELECTOR = '[name="members-manager"]';

export const MANAGER_MEMBERS_WRAP_DATALIST_SELECTOR =
  '#members-manager-wrap-datalist';

export const MANAGER_MEMBERS_DATALIST_ID = 'members-manager-datalist';

export const MANAGER_MEMBERS_LIST_ID = 'members-manager-list';

export const MANAGER_MEMBERS_SELECTED_SELECTOR = '#members-manager-selected';

export const ASSIGNEE_LIST_SELECTOR =
'#peopledetails > dl:nth-child(1) > dd';

export const SETTINGS_ID = 'settings';

export const SETTINGS_FORM_SELECTOR = '#settings form';