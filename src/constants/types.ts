export const COMMIT = 'COMMIT';

export const BRANCH = 'BRANCH';

export const SETTING = 'SETTING';

export const TASK_TYPE_BUG = 'BUG';

export const COMMIT_TYPE_BUG = 'FIX';

export const COMMIT_TYPE_FEATURE = 'ADD';

export const BRANCH_TYPE_BUG = 'bugfix';

export const BRANCH_TYPE_FEATURE = 'feature';

export const PROJECT_UPDATE_EVENT = 'user-scripts-project-update';

export const MEMBER_ACTION_ADD = 'ADD';

export const MEMBER_ACTION_REMOVE = 'REMOVE';
