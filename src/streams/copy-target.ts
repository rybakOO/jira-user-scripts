import { fromEvents, merge } from 'kefir';
import { IDescription } from 'interfaces/decription';
import getIconClass from 'services/icon-class';
import { insertElementById } from 'services/dom';

const getCopyTargetListener = (items: IDescription[], headerEl: HTMLElement) => {

  const elements = items.map((item) => insertElementById(
    `copy-target-${item.type.toLowerCase()}`,
    {
      content: item.content,
      iconClass: getIconClass(item.type),
    },
    'button',
    headerEl
  ));

  return merge(elements.map((element) => fromEvents<MouseEvent, any>(element as HTMLElement, 'click')
  .filter(
    (event: MouseEvent) =>
      event.currentTarget instanceof HTMLElement &&
      event.currentTarget.hasAttribute('title')
  )
  .map((event: MouseEvent) => {
    const target = event.currentTarget as HTMLElement;
    return target.getAttribute('title');
  })));
};

export default getCopyTargetListener;
