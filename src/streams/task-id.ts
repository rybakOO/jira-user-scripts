import { constant } from 'kefir';
import { TASK_ID_SELECTOR } from 'constants/selectors';
import { getElement } from 'streams/dom';

const taskId = () =>
  getElement(TASK_ID_SELECTOR)
    .map((el: HTMLInputElement) => el && el.value)
    .toProperty();

export default taskId;
