import { constant } from 'kefir';
import { getTextContentBySelector } from 'utils/dom';
import { TASK_KEY_SELECTOR } from 'constants/selectors';

const taskKey = () =>
  constant(getTextContentBySelector(TASK_KEY_SELECTOR)).toProperty();

export default taskKey;
