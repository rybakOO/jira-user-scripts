import { constant, stream, merge, sequentially, constantError } from 'kefir';
import ajax from 'streams/ajax';

const fromEvents = jest
  .fn()
  .mockImplementation(() => sequentially(100, ['text']));
jest.mock('kefir', () => ({
  fromEvents: fromEvents,
  stream,
  merge,
  constant,
}));

import getSearchMembers from './search-members';

const defaultValue = undefined;
const params = {
  param1: 'param1',
  param2: 'param2',
};
const error = 'Something wrong!';
const inputEl = document.createElement('input');
const members = [
  {
    name: 'name',
  },
];

// @ts-ignore
ajax.mockImplementation(() => constant(members));

const mockLoading = jest.fn(value => {
  expect(value).toMatchObject({
    type: 'loading',
  });
});
const mockResult = jest.fn(value => {
  expect(value).toMatchObject({
    type: 'result',
    items: members,
  });
});
const mockError = jest.fn(value => {
  expect(value).toMatchObject({
    type: 'error',
    error,
  });
});

describe('Search members service works properly', () => {
  beforeEach(() => {
    mockLoading.mockClear();
    mockResult.mockClear();
    mockError.mockClear();
  });

  test('should be returned result properly', async () => {
    await new Promise(resolve => {
      const searchMembers$ = getSearchMembers(inputEl, params, defaultValue);

      searchMembers$
        .filter(({ type }) => type === 'loading')
        .onValue(mockLoading);

      searchMembers$
        .filter(({ type }) => type === 'result')
        .onValue(value => {
          mockResult(value);
          resolve();
        });

      setTimeout(() => {
        resolve();
      }, 500);
    });

    expect(mockLoading).toBeCalled();
    expect(mockResult).toBeCalled();
  });

  test('should be returned error properly', async () => {
    // @ts-ignore
    ajax.mockImplementation(() => constantError(error));

    await new Promise(resolve => {
      const searchMembers$ = getSearchMembers(inputEl, params, defaultValue);

      searchMembers$
        .filter(({ type }) => type === 'loading')
        .onValue(mockLoading);

      searchMembers$.filter(({ type }) => type === 'error').onValue(mockError);

      setTimeout(() => {
        resolve();
      }, 500);
    });

    expect(mockLoading).toBeCalled();
    expect(mockError).toBeCalled();
  });
});
