import ajax from 'streams/ajax';
import { merge, constant } from 'kefir';

const getProjects = () => {
  const request$ = ajax('/secure/QuickCreateIssue!default.jspa?decorator=none');

  return merge([
    request$
      .map((data: any) => {
        const fragment = document.createElement('div');
        fragment.innerHTML = data.fields[0].editHtml;
        const projectsEl = fragment.querySelector('[data-suggestions]');
        return JSON.parse(projectsEl.getAttribute('data-suggestions'));
      })
      // get whole list of user's projects
      .map(projects => projects[projects.length - 1].items),
    request$
      .ignoreValues()
      .flatMapErrors(constant)
      .map(error => ({
        type: 'error',
        error,
      })),
  ]).toProperty();
};

export default getProjects;
