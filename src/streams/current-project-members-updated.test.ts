import { constant } from 'kefir';
import membersStore from 'services/members-store';
import getProjectId from 'streams/project-id';
import { PROJECT_UPDATE_EVENT } from 'constants/types';
import currentProjectMembersUpdated from './current-project-members-updated';
import members from '../../tests/data/members';

const projectId = '12345';
// @ts-ignore
getProjectId.mockImplementation(() => constant(projectId));

const event = new Event(PROJECT_UPDATE_EVENT);

it("Current project's members updated is worked correctly", () => {
  currentProjectMembersUpdated.onValue(value => {
    expect(membersStore.getMembersByProjectId).toBeCalledWith(projectId);
    expect(value).toMatchObject(members);
  });

  window.dispatchEvent(event);

  expect(membersStore.getMembersByProjectId).toBeCalledTimes(2);
});
