import ajax from './ajax';

const result = {
  properly: 'properly',
  properly1: 'properly1',
};
const resultError = {
  status: 100,
  responseText: 'response text',
};
const doneMock = jest.fn(fn => fn(result));
const failMock = jest.fn(fn => fn(resultError));
const abortMock = jest.fn();

const ajaxMockSuccess = jest.fn(() => ({
  done: doneMock,
  fail: jest.fn(),
  abort: abortMock,
}));

const ajaxMockFail = jest.fn(() => ({
  done: jest.fn(),
  fail: failMock,
  abort: abortMock,
}));

Object.defineProperty(window, '$', {
  writable: true,
  value: {
    ajax: ajaxMockSuccess,
  },
});

const options = {
  url: '/v3/members/1248',
};

describe('Ajax stream works properly', () => {
  it('Success case has been completed properly', () => {
    const ajax$ = ajax(options).onValue(_result => {
      expect(ajaxMockSuccess).toBeCalled();
      expect(doneMock).toBeCalled();
      expect(abortMock).toBeCalled();
      expect(_result).toMatchObject(result);
    });

    ajaxMockSuccess.mockClear();
    doneMock.mockClear();
    abortMock.mockClear();
    ajax$.onValue(_result => {
      expect(ajaxMockSuccess).not.toBeCalled();
      expect(doneMock).not.toBeCalled();
      expect(abortMock).not.toBeCalled();
      expect(_result).toMatchObject(result);
    });
  });

  it('Fail case has been completed properly', () => {
    Object.defineProperty(window, '$', {
      writable: true,
      value: {
        ajax: ajaxMockFail,
      },
    });

    ajax(options).onError(_result => {
      expect(ajaxMockFail).toBeCalled();
      expect(failMock).toBeCalled();
      expect(abortMock).toBeCalled();
      expect(_result).toBe(resultError.responseText);
    });
  });
});
