import { constant } from 'kefir';
import {
  getElement,
  watchForExistDOMElement,
  handleEventElement,
  getAttribute,
} from './dom';

const consoleWarn = jest.fn();

window.console.warn = consoleWarn;

describe('DOM streams work properly', () => {
  beforeEach(() => {
    document.body.innerHTML = '';
    consoleWarn.mockClear();
  });

  test('getElement works correctly', () => {
    document.body.innerHTML = '<div id="container"></div>';
    getElement('#container').onValue((el: HTMLElement) => {
      expect(el.id).toBe('container');
    });
  });

  test('watchForExistDOMElement works correctly', () => {
    let target;
    document.body.innerHTML = '<div id="container"></div>';
    watchForExistDOMElement('#container', '#target').onValue((value: any) => {
      const _target = value.target as HTMLElement;
      expect(_target.outerHTML).toBe(target.outerHTML);
    });
    const container = document.body.querySelector('#container');
    container.innerHTML = '<div id="target"></div>';
    target = container.querySelector('#target');

    watchForExistDOMElement('#failcontainer', '#target').observe();
    expect(consoleWarn).toBeCalled();
  });

  test('handleEventElement works correctly', () => {
    const el = document.createElement('div');
    const target = document.createElement('div');
    const fromEvents = jest.fn(() =>
      constant({
        target,
      })
    );
    jest.mock('kefir', () =>
      jest.fn().mockImplementation(() => ({ fromEvents }))
    );
    handleEventElement(el, 'click').onValue(_target => {
      expect(fromEvents).toBeCalledWith(el, 'click');
      expect(_target).toBe(target);
    });
  });

  test('getAttribute works correctly', () => {
    const el = document.createElement('div');
    el.title = 'lorem';

    getAttribute(constant(el), 'title').onValue(attr => {
      expect(attr).toBe('lorem');
    });
  });
});
