import { stream } from 'kefir';
import searchMembers from 'streams/search-members';
import { handleEventElement, getAttribute } from 'streams/dom';
import IProject from 'interfaces/project';
import IMember from 'interfaces/member';
import MembersManager from 'components/MembersManager';
import { MEMBER_ACTION_ADD, MEMBER_ACTION_REMOVE } from 'constants/types';

const initAssineeSettings = ([container, project, issueKey, enabledMembers]: [
  HTMLElement,
  IProject,
  string,
  IMember[]
]) => {
  const membersManager = new MembersManager(container, project, enabledMembers);
  const isMembersManagerExist = Boolean(membersManager.members.length);

  return stream(emitter => {
    if (isMembersManagerExist) {
      initDatalist(membersManager.members);
    }

    searchMembers(
      membersManager.elements.searchInput,
      {
        issueKey,
        maxResults: 10,
        startAt: 0,
      },
      isMembersManagerExist ? undefined : ''
    ).onValue((data: any) => {
      membersManager.hideLoader();

      switch (data.type) {
        case 'loading':
          membersManager.showLoader();
          break;
        case 'result':
          initDatalist(data.items);
          break;
        case 'error':
          console.error(data);
          membersManager.renderDetaList({
            error: true,
          });
          break;
      }
    });

    getAttribute(
      handleEventElement(membersManager.elements.memberList, 'click'),
      'data-member-key'
    )
      .map(memberKey => enabledMembers.find(member => member.key === memberKey))
      .onValue((member: IMember) =>
        emitter.emit({
          type: MEMBER_ACTION_REMOVE,
          member,
          projectId: project.value,
        })
      );

    function initDatalist(members: IMember[]) {
      membersManager.members = members;
      const datalist = membersManager.renderDetaList(members);

      getAttribute(handleEventElement(datalist, 'click'), 'data-member-key')
        .map(memberKey => members.find(member => member.key === memberKey))
        .onValue((member: IMember) =>
          emitter.emit({
            type: MEMBER_ACTION_ADD,
            member,
            projectId: project.value,
          })
        );
    }
  });
};

export default initAssineeSettings;
