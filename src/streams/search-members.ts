import { fromEvents, constant, merge } from 'kefir';
import { TIME_WAITE_DEBOUNCE } from 'constants/index';
import ajax from 'streams/ajax';

// General utility functions

// Returns a Property containig current input value
const inputValue = input => {
  const getValue = () => {
    return input.value;
  };
  return fromEvents(input, 'input', getValue).debounce(TIME_WAITE_DEBOUNCE);
};

// Less general utility functions
const search = (queryStr: string, params = {}) =>
  ajax({
    url: '/rest/api/latest/user/assignable/search',
    data: {
      username: queryStr,
      ...params,
    },
  });

export default (
  inputEl: HTMLInputElement,
  params: any,
  defaultValue?: string
) => {
  const searchQuery$ = merge([
    inputValue(inputEl),
    constant(defaultValue)
      .toProperty()
      .filter(value => value !== undefined),
  ]).toProperty();

  const searchResult$ = searchQuery$.flatMap((queryStr: any) =>
    search(queryStr, params)
  );

  return merge([
    searchQuery$.map(() => ({ type: 'loading' })),
    searchResult$.map((items: any) => ({
      type: 'result',
      items,
    })),
    searchResult$
      .ignoreValues()
      .flatMapErrors(constant)
      .map(error => ({
        type: 'error',
        error,
      })),
  ]).toProperty();
};
