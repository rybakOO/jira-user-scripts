import { getTextContentBySelector } from '../utils/dom';
import { getTextContentBySelector as getTextContentBySelectorMock } from 'utils/dom';
import getTaskKey from './task-key';

const taskKey = 'MC-000000';

document.body.innerHTML = `
  <div id="key-val">${taskKey}</div>
`;

// @ts-ignore
getTextContentBySelectorMock.mockImplementation((...args) =>
  // @ts-ignore
  getTextContentBySelector(...args)
);

it('Task key stream works properly', () => {
  getTaskKey().onValue(_taskKey => {
    expect(taskKey).toBe(_taskKey);
  });
});
