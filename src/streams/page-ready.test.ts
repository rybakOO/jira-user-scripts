import { constant } from 'kefir';
import pageReady from './page-ready';
import { getElement, watchForExistDOMElement } from 'streams/dom';
import {
  MAIN_CONTAINER_SELECTOR,
  HEADER_MAIN_SELECTOR,
} from 'constants/selectors';

// @ts-ignore
getElement.mockImplementation(() => constant(true));
// @ts-ignore
watchForExistDOMElement.mockImplementation(() => constant(true));

it('Page ready stream works properly', () => {
  pageReady().onValue(value => {
    expect(value).toBeTruthy();
    expect(getElement).toBeCalled();
    expect(watchForExistDOMElement).not.toBeCalled();
  });

  // @ts-ignore
  getElement.mockImplementation(() => constant(false));

  pageReady().onValue(value => {
    expect(value).toBeTruthy();
    expect(getElement).toBeCalled();
    expect(watchForExistDOMElement).toBeCalledWith(
      MAIN_CONTAINER_SELECTOR,
      HEADER_MAIN_SELECTOR
    );
  });
});
