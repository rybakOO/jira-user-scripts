import { combine } from 'kefir';
import IProject from 'interfaces/project';

const getCurrentProject = (projects$, projectId$) =>
  combine(
    [projects$, projectId$],
    (projects: IProject[], currentProjectId: string) =>
      projects.find((project: IProject) => project.value === currentProjectId)
  ).toProperty();

export default getCurrentProject;
