import { stream } from 'kefir';

const refreshPage$ = stream(emitter => {
  const fn = () => {
    emitter.emit(JIRA.Events.ISSUE_REFRESHED);
  };

  JIRA.bind(JIRA.Events.ISSUE_REFRESHED, fn);

  return () => {
    JIRA.unbind(JIRA.Events.ISSUE_REFRESHED, fn);
  };
});

export default refreshPage$;
