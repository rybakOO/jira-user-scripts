import { stream, fromEvents } from 'kefir';
import membersStore from 'services/members-store';
import { PROJECT_UPDATE_EVENT } from 'constants/types';
import projectId from 'streams/project-id';

const currentProjectMembersUpdated$ = stream(emitter => {
  const emitMembers = () => {
    projectId().onValue(async projectId => {
      emitter.emit(await membersStore.getMembersByProjectId(projectId));
    });
  };

  fromEvents(window, PROJECT_UPDATE_EVENT, () => {
    emitMembers();
  }).observe();

  emitMembers();

  return () => {
    emitter.end();
  };
}).toProperty();

export default currentProjectMembersUpdated$;
