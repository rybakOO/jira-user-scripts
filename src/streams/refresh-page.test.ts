import refreshPage$ from './refresh-page';

let callback;
const bind = jest.fn((eventName, fn) => {
  callback = fn;
  fn();
});
const unbind = jest.fn();

Object.defineProperty(window, 'JIRA', {
  writable: true,
  value: {
    bind,
    unbind,
    Events: {
      ISSUE_REFRESHED: 'ISSUE_REFRESHED',
    },
  },
});

it('Refresh page works properly', () => {
  const onValue = jest.fn();

  refreshPage$.onValue(onValue);

  expect(onValue).toBeCalled();
  expect(bind).toBeCalledWith('ISSUE_REFRESHED', callback);
});
