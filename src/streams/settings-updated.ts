import { stream, fromEvents } from 'kefir';
import store from 'services/members-store';
import { PROJECT_UPDATE_EVENT } from 'constants/types';
import projectId from 'streams/project-id';
import { BRANCH_TYPE_BUG, BRANCH_TYPE_FEATURE } from '../constants/types';

const settingsUpdated$ = stream(emitter => {
  const emitSettings = () => {
    projectId().onValue(async projectId => {
      const result = await store.getSettingsByProjectId(projectId);

      emitter.emit(result.length ? result[0] : {
        prefixBranchBugs: BRANCH_TYPE_BUG,
        prefixBranchFeatures: BRANCH_TYPE_FEATURE,
      });
    });
  };

  fromEvents(window, PROJECT_UPDATE_EVENT, () => {
    emitSettings();
  }).observe();

  emitSettings();

  return () => {
    emitter.end();
  };
}).toProperty();

export default settingsUpdated$;
