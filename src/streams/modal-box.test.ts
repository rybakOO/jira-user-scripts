import { constant, stream } from 'kefir';
import { insertElementById } from 'services/dom';
import getIconClass from 'services/icon-class';

const fromEvents = jest.fn().mockImplementation(() => constant(null));
jest.mock('kefir', () => ({
  fromEvents: fromEvents,
  stream,
}));

import getModalBox from './modal-box';

const container = document.createElement('div');
const element = document.createElement('div');
const icon = 'BRANCH';
const id = 'modal-target-test';
let fancyboxConfig;
const fancybox = jest.fn((template, config) => {
  document.body.innerHTML = template;
  fancyboxConfig = config;
});

// @ts-ignore
insertElementById.mockImplementation(() => element);

Object.defineProperty(window, '$', {
  writable: true,
  value: {
    fancybox,
  },
});

it('Modal box stream works properly', () => {
  const fn = jest.fn(el => {
    expect(el).toBeInstanceOf(HTMLElement);
  });
  getModalBox(container, icon, id).onValue(fn);

  expect(insertElementById).toBeCalledWith(
    id,
    {
      iconClass: getIconClass(icon),
      title: 'Settings of a user scripts',
    },
    'modalBox',
    container,
    'afterbegin'
  );
  expect(fromEvents).toBeCalledWith(element, 'click');

  fancyboxConfig.onComplete();
  expect(fn).toBeCalled();
  expect(fancyboxConfig.modal).toBe(false);
  expect(fancyboxConfig.overlayShow).toBe(true);
  expect(fancyboxConfig.enableEscapeButton).toBe(true);
  expect(fancyboxConfig.width).toBe(600);
});
