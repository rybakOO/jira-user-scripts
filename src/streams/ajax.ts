import { stream } from 'kefir';

// Accepts `jQuery.ajax` options.
// Returns a Propery that will contain single value or error.
// The request is send at the moment the property gets a subscriber,
// and if it loses all subscribers before the response, the request will be canceled.
const ajax = options =>
  stream(emitter => {
    const jqXHR = $.ajax(options);
    jqXHR.done(emitter.emit);
    jqXHR.fail((jqXHR, textStatus, errorThrown) => {
      emitter.error(
        jqXHR.status === 0 ? 'Connection problem' : jqXHR.responseText
      );
    });
    return () => {
      jqXHR.abort();
    };
  })
    .take(1)
    .takeErrors(1)
    .toProperty();

export default ajax;
