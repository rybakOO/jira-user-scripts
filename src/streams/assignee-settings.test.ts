import { stream, constant } from 'kefir';
import initAssineeSettings from './assignee-settings';
import MembersManger, {
  // @ts-ignore
  renderDetaList,
  // @ts-ignore
  showLoader,
  // @ts-ignore
  hideLoader,
  // @ts-ignore
  elements,
  // @ts-ignore
  mockMembersManager,
} from 'components/MembersManager';
import searchMembers from 'streams/search-members';
import demoMembers from '../../tests/data/members';
import IProject from 'interfaces/project';
import IMember from 'interfaces/member';
import { getAttribute, handleEventElement } from 'streams/dom';

const container: HTMLElement = document.createElement('div');
const project: IProject = {
  label: 'test project',
  value: 'test project value',
  icon: 'test project icon',
};
const issueKey: string = '00000';
const enabledMembers: IMember[] = demoMembers.slice(0, 1);

describe('Assignee settings streem works properly', () => {
  beforeEach(() => {
    // @ts-ignore
    MembersManger.mockClear();
    renderDetaList.mockClear();
    mockMembersManager.members = [];
  });

  it('should initialization properly', () => {
    initAssineeSettings([container, project, issueKey, enabledMembers]);
    expect(MembersManger).toBeCalledWith(container, project, enabledMembers);
  });

  it('should emit loading members', () => {
    const stream$ = initAssineeSettings([
      container,
      project,
      issueKey,
      enabledMembers,
    ]);

    stream$.observe();

    expect(searchMembers).toBeCalledWith(
      elements.searchInput,
      {
        issueKey,
        maxResults: 10,
        startAt: 0,
      },
      ''
    );
    expect(hideLoader).toBeCalled();
    expect(showLoader).toBeCalled();
  });

  it('should emit adding a member to enabled list properly', () => {
    const member = demoMembers[0];
    const memberKey = member.key;
    // Needed reject an event's of removing member from the list
    // @ts-ignore
    getAttribute.mockImplementation(value =>
      stream(emmiter => {
        if (value === 'datalist') {
          emmiter.emit(memberKey);
        } else {
          emmiter.end();
        }
      })
    );

    // @ts-ignore
    handleEventElement.mockImplementation(value => value);

    // @ts-ignore
    renderDetaList.mockImplementation(() => 'datalist');

    // @ts-ignore
    searchMembers.mockImplementation(() =>
      constant({
        type: 'result',
        items: demoMembers,
      })
    );

    const stream$ = initAssineeSettings([
      container,
      project,
      issueKey,
      enabledMembers,
    ]);

    stream$.onValue(result => {
      expect(renderDetaList).toBeCalledWith(demoMembers);
      expect(mockMembersManager.members).toMatchObject(demoMembers);
      expect(result).toMatchObject({
        type: 'ADD',
        member,
        projectId: project.value,
      });
    });
  });

  it('should emit removing a member from enabled list properly', () => {
    const member = demoMembers[0];
    const memberKey = member.key;

    // @ts-ignore
    getAttribute.mockImplementation(() => constant(memberKey));

    // @ts-ignore
    searchMembers.mockImplementation(() => stream(() => {}));

    const stream$ = initAssineeSettings([
      container,
      project,
      issueKey,
      enabledMembers,
    ]);

    stream$.onValue(result => {
      expect(result).toMatchObject({
        type: 'REMOVE',
        member,
        projectId: project.value,
      });
    });
  });

  it('behavior is correct when MembersManager has already been exist', () => {
    const member = demoMembers[0];
    const memberKey = member.key;
    const datalist = demoMembers.slice(0, 2);
    mockMembersManager.members = datalist;
    // Needed reject an event's of removing member from the list
    // @ts-ignore
    getAttribute.mockImplementation(value =>
      stream(emmiter => {
        if (value === 'datalist') {
          emmiter.emit(memberKey);
        } else {
          emmiter.end();
        }
      })
    );

    // @ts-ignore
    handleEventElement.mockImplementation(value => value);

    // @ts-ignore
    renderDetaList.mockImplementation(() => 'datalist');

    // @ts-ignore
    searchMembers.mockImplementation(() => stream(() => {}));

    const stream$ = initAssineeSettings([
      container,
      project,
      issueKey,
      enabledMembers,
    ]);

    stream$.onValue(result => {
      expect(renderDetaList).toBeCalledWith(datalist);
      expect(mockMembersManager.members).toMatchObject(datalist);
      expect(result).toMatchObject({
        type: 'ADD',
        member,
        projectId: project.value,
      });
    });
  });

  it('should handle errors properly', () => {
    const errorMock = (window.console.error = jest.fn());
    const error = {
      type: 'error',
      message: 'some error',
    };
    // Needed reject an event's of removing member from the list
    // @ts-ignore
    getAttribute.mockImplementation(() => stream(() => {}));

    // @ts-ignore
    handleEventElement.mockImplementation(value => value);

    // @ts-ignore
    renderDetaList.mockImplementation(value => value);

    // @ts-ignore
    searchMembers.mockImplementation(() => constant(error));

    const stream$ = initAssineeSettings([
      container,
      project,
      issueKey,
      enabledMembers,
    ]);

    stream$.observe();

    expect(renderDetaList).toBeCalledWith({
      error: true,
    });
    expect(errorMock).toBeCalledWith(error);
  });
});
