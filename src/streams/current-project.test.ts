import { constant } from 'kefir';
import getCurrentProject from './current-project';

const projectId = '12345';
const projects = [
  {
    label: 'string',
    value: projectId,
    icon: 'string',
  },
  {
    label: 'string',
    value: '54321',
    icon: 'string',
  },
];

it('Current project stream works properly', () => {
  const currentProject$ = getCurrentProject(
    constant(projects),
    constant(projectId)
  );
  currentProject$.onValue(value => {
    expect(value).toBe(projects[0]);
  });
  currentProject$.onValue(value => {
    expect(value).toBe(projects[0]);
  });
});
