import { getElement } from './dom';
import { getElement as getElementMock } from 'streams/dom';
import getTaskId from './task-id';

const taskId = 'MC-000000';

document.body.innerHTML = `
  <input type="hidden" name="id" value="${taskId}"></div>
`;

// @ts-ignore
getElementMock.mockImplementation((...args) => getElement(...args));

it('Task id stream works properly', () => {
  getTaskId().onValue(_taskId => {
    expect(taskId).toBe(_taskId);
  });
});
