import { getElement } from 'streams/dom';
import { TOKEN_SELECTOR } from 'constants/selectors';

const token$ = getElement(TOKEN_SELECTOR)
  .map((el: HTMLElement) => el && el.getAttribute('content'))
  .toProperty();

export default token$;
