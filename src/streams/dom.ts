import { stream, fromEvents } from 'kefir';

export const getElement = (selector: string) =>
  stream(emitter => {
    emitter.emit(document.querySelector(selector));
  });

export const watchForExistDOMElement = (
  containerSelector: string,
  targetSelector: string
) =>
  stream(emitter => {
    const checkExist = () => {
      const target = container.querySelector(targetSelector);
      if (target) {
        emitter.emit({
          target,
          value: true,
        });
      }
      return Boolean(target);
    };
    const container: HTMLElement = document.querySelector(containerSelector);

    if (!container) {
      console.warn(
        `[watchForExistDOMElement] ${containerSelector} has not been found!`
      );
      return;
    }
    if (checkExist()) {
      return;
    }
    const observerInstance = new MutationObserver(mutationsList => {
      for (let mutation of mutationsList) {
        if (mutation.type === 'childList') {
          checkExist();
        }
      }
    });

    observerInstance.observe(container, {
      attributes: false,
      childList: true,
      subtree: true,
    });

    return () => {
      observerInstance.disconnect();
    };
  }).take(1);

export const handleEventElement = (el: HTMLElement, eventName: string) =>
  fromEvents(el, eventName).map(
    (event: MouseEvent) => event.target as HTMLButtonElement
  );

export const getAttribute = (stream: any, attr: string) =>
  stream
    .filter((target: HTMLButtonElement) => target.hasAttribute(attr))
    .map((target: HTMLButtonElement) => target.getAttribute(attr));
