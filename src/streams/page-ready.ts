import { constant } from 'kefir';
import { getElement, watchForExistDOMElement } from 'streams/dom';
import {
  HEADER_MAIN_SELECTOR,
  MAIN_CONTAINER_SELECTOR,
} from 'constants/selectors';

const pageReady = () =>
  getElement(HEADER_MAIN_SELECTOR)
    .map(Boolean)
    .flatMap(isTaskPageWithoutAside =>
      isTaskPageWithoutAside
        ? constant(true)
        : watchForExistDOMElement(MAIN_CONTAINER_SELECTOR, HEADER_MAIN_SELECTOR)
    )
    .toProperty();

export default pageReady;
