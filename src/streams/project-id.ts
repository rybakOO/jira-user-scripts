import { getElement } from 'streams/dom';
import { PROJECT_ID_SELECTOR } from 'constants/selectors';

const projectId = () =>
  getElement(PROJECT_ID_SELECTOR)
    .map((el: HTMLElement) => el && el.getAttribute('data-project-id'))
    .toProperty();

export default projectId;
