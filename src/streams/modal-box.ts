import { fromEvents, stream } from 'kefir';
import getIconClass from 'services/icon-class';
import { insertElementById } from 'services/dom';

const createModalBox = (
  container: HTMLElement,
  icon: string,
  id: string = 'modal'
) => {
  const element = insertElementById(
    id,
    {
      iconClass: getIconClass(icon),
      title: 'Settings of a user scripts',
    },
    'modalBox',
    container,
    'afterbegin'
  );

  return fromEvents(element, 'click')
    .flatMap(() =>
      stream<HTMLElement, never>(emitter => {
        const id = `modal-${new Date().getTime()}-box`;
        // @ts-ignore
        $.fancybox(
          `<div id="${id}" class="user-script-modal-box">
            <div class="loader"><div>
          </div>`,
          {
            modal: false,
            overlayShow: true,
            enableEscapeButton: true,
            width: 600,
            onComplete: () => {
              emitter.emit(document.getElementById(id));
            },
          }
        );
      })
    )
    .toProperty();
};

export default createModalBox;
