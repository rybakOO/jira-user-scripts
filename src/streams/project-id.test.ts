import { getElement } from './dom';
import { getElement as getElementMock } from 'streams/dom';
import getProjectId from './project-id';

const projectId = 'MC-000000';

document.body.innerHTML = `
  <div data-project-id="${projectId}"></div>
`;

// @ts-ignore
getElementMock.mockImplementation((...args) => getElement(...args));

it('Project id stream works properly', () => {
  getProjectId().onValue(_projectId => {
    expect(projectId).toBe(_projectId);
  });
});
