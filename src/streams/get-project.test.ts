import { constant } from 'kefir';
import ajax from 'streams/ajax';
import getProjects from './get-project';
import projectsTemplate, { result } from '../../tests/data/projects';

// @ts-ignore
ajax.mockImplementation(() => constant(projectsTemplate));

it('getProject works properly', () => {
  const projects$ = getProjects().onValue(value => {
    expect(value).toMatchObject(result);
  });
  projects$.onValue(value => {
    expect(value).toMatchObject(result);
  });
});
