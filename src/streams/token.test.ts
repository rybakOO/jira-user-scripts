import { getElement } from './dom';
import { getElement as getElementMock } from 'streams/dom';
// @ts-ignore
getElementMock.mockImplementation((...args) => getElement(...args));

import token$ from './token';

const token = '000000-000000-000000-000000';

document.body.innerHTML = `
  <meta id="atlassian-token" content="${token}" />
`;

it('Task id stream works properly', () => {
  token$.onValue(_token => {
    expect(token).toBe(_token);
  });
});
