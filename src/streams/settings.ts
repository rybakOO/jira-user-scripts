import { fromEvents } from 'kefir';
import ISettings from 'interfaces/settings';
import SettingsManager from '../components/SettingsManager';
import IProject from 'interfaces/project';

const initSettings = ([[container, project], settings]: [
  [HTMLElement, IProject],
  ISettings
]) => {

  const settingsManager = new SettingsManager(container, settings);
  return fromEvents(settingsManager.elements.form, 'submit')
    .map((event:Event): { settings:ISettings, projectId: string } => {
      event.preventDefault();
      try {
        const form: HTMLFormElement = event.target as HTMLFormElement;
        return {
          projectId: project.value,
          settings: {
            prefixBranchBugs: form.elements['prefix-branch-bugs'].value.trim(),
            prefixBranchFeatures: form.elements['prefix-branch-features'].value.trim(),
          }
        } 
      } catch(e) {
        console.error('[initSettings]', e);
      }
    })
};

export default initSettings;
