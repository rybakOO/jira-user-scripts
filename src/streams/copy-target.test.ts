import { constant } from 'kefir';
import getCopyTargetListener from './copy-target';
import { IDescription } from 'interfaces/decription';
import { insertElementById } from 'services/dom';
import getIconClass from 'services/icon-class';

const descriptions: IDescription[] = [
  {
    type: 'BRANCH',
    content: 'description',
  },
  {
    type: 'COMMIT',
    content: 'COMMIT-description',
  }
];

const targetElementText = 'description'
const element = document.createElement('div');

const currentTarget = document.createElement('div');
currentTarget.setAttribute('title', targetElementText);

const fromEvents = jest.fn(() =>
  constant({
    currentTarget,
  })
);

jest.mock('kefir', () => jest.fn().mockImplementation(() => ({ fromEvents })));

// @ts-ignore
insertElementById.mockImplementation(() => element);

it('Copy Target stream works properly', () => {
  const copyTarget$ = getCopyTargetListener(descriptions, element);
  
  expect(insertElementById).toBeCalledWith(
    `copy-target-${descriptions[0].type.toLowerCase()}`,
    {
      content: descriptions[0].content,
      iconClass: getIconClass(descriptions[0].type),
    },
    'button',
    element
  );
  copyTarget$.onValue(value => {
    expect(fromEvents).toBeCalledWith(element, 'click');
    expect(value).toBe(descriptions[0].content);
  });
});
